define(['app/app','text!app/layouts/main-layout.html', 'marionette'], function (app, template, Marionette) {
    return Marionette.Layout.extend({
        template: template,

        regions: {
            top: "#top",
            bottom: "#bottom",
            left: "#left",
            right: "#right"
        },

        closeAll: function () {
            _.each(this.regions, function (id, key) {
                app.mainLayout[key].close();
            });
        }

    });
});
