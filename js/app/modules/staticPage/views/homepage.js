define(['app/app','text!modules/staticPage/templates/homepage.html','marionette'], function (app, template, Marionette) {
    return Marionette.ItemView.extend({
        template: template
    });
});