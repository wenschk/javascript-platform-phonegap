define(
    [
        'app/app',
        'modules/staticPage/views/about',
        'marionette'
    ],
    function (
        app,
        aboutPageView,
        Marionette
        )
    {
        return Marionette.Controller.extend({

            home: function () {
                app.mainLayout.top.show(app.module("staticPage").defaultView());
            },
            about: function () {
                app.mainLayout.top.show(new aboutPageView());
            }
        });
    }
);
