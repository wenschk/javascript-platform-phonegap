/**
 * Created with IntelliJ IDEA.
 * Date: 2.2.2014
 * Time: 13:42
 * To change this template use File | Settings | File Templates.
 */
define(['app/app', 'modules/staticPage/controller',    'modules/staticPage/views/homepage'  , 'marionette'], function(app, mainController, defaultView, Marionette) {

    app.module("staticPage", function(home){

        var router = null ;

        home.startWithParent = true;
        
         home.defaultView = function(){
           return new defaultView ();
        };       
        
        home.addInitializer(function(){
            router = new Marionette.AppRouter({
                controller: new mainController(),
                appRoutes: {
                    "/": 'home',
                    "": 'home',
                    "home": 'home',
                    "about": 'about'
                }
            });



        });


    });


    return app;
});



