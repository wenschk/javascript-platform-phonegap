define(['app/app', 'text!modules/navigation/navigation.html','marionette'], function (app, template, Marionette) {
    return Marionette.ItemView.extend({
        className: 'navbar-inner',
        template: template,

        events: {
            'click .nav a' : 'navigate'
        },

        ui: {
            links: '.nav li'
        },
        navigate: function (e) {
            var link = $(e.currentTarget),
                page = link.attr('href').replace(/#/gi, '');

            this.ui.links.removeClass("active");
            link.closest('li').addClass("active");

            app.vent.trigger('navigate', page);
            return false;
        }
    });
});