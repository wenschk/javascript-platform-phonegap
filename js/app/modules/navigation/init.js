/**
 * Created with IntelliJ IDEA.
 * User: kennis
 * Date: 2.2.2014
 * Time: 13:42
 * To change this template use File | Settings | File Templates.
 */
define(['app/app','modules/navigation/view'], function(app, View) {

    app.module("navigation", function(navigation){

        navigation.startWithParent = true;

        navigation.addInitializer(function(){
            app.vent.on("navigate:from", this.navigateFrom, this);
            app.vent.on("navigate:to", this.navigateTo, this);
            app.vent.on("navigate", this.navigate, this);
        });
        navigation.View = function(){
           return new View ();
        };
        navigation.navigate = function (page) {
            app.vent.trigger('navigate:from', Backbone.history.fragment);
            app.vent.trigger('navigate:to', page);
        };

        navigation.navigateFrom = function (page) {

        };

        navigation.navigateTo = function (page) {

            if(("/" + Backbone.history.fragment) != page && page!="home"){
                app.mainLayout.closeAll();
                Backbone.history.navigate(page, {trigger: true});
            }

            if($('.in').is(":visible")){
                $('.navbar-toggle').click();
            }
        }

    });


    return app;
});



