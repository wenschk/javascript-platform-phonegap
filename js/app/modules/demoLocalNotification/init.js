define([
    'app/app', 
    'modules/demoLocalNotification/controller', 
    'marionette'
], function(app, mainController, Marionette) {

    app.module("demoLocalNotification", function(localNotification){

        var router = null ;
        localNotification.startWithParent = true;

        localNotification.addInitializer(function(){
            router = new Marionette.AppRouter({
                controller: new mainController(),
                appRoutes: {
                    "demoLocalNotification": 'start'
                }
            });

        });


    });


    return app;
});



