define(
    [
        'app/app',
        'modules/demoLocalNotification/views/noticiation',
        'marionette',
        'system/libraries/phonegap/local-noticiation',
    ],
    function (
        app,
        testPageView,
        Marionette,
        notification        
        )
    {
        return Marionette.Controller.extend({

            start: function () {
                //notification.init();
                //notification.add();
                app.mainLayout.top.show(new testPageView());
               
            }
        });
    }
);
