define(
    [   'app/app',
        'text!modules/demoLocalNotification/templates/testpage.html',
        'marionette'
    ],
    function (app, template, Marionette) {
    return Marionette.ItemView.extend({
        template: template

    });
});