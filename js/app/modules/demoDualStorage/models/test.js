/**
 * Created with IntelliJ IDEA.
 * User: kennis
 * Date: 8.2.2014
 * Time: 14:20
 * To change this template use File | Settings | File Templates.
 */
define(['app/app', 'backbone'], function (app, Backbone) {
    return Backbone.Model.extend({
        local: true,
        url: "test",
        storeName: "testuser",
        defaults: {
            name: ''

        }
    });
});