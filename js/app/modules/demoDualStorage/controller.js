define(
    [
        'app/app',
        'modules/demoDualStorage/views/testpage',
        'modules/demoDualStorage/models/test',
        'marionette'
    ],
    function (
        app,
        testPageView,
        testModel,
        Marionette
        )
    {
        return Marionette.Controller.extend({

            test: function () {

                var test = new testModel();
               // app.mainLayout.top.show(new testPageView({model: test}));

                test.fetch({
                    success: function(){
                        app.mainLayout.top.show(new testPageView({model: test}));
                    },
                    error: function(){
                        app.mainLayout.top.show(new testPageView({model: test}));
                     }
                });

            }
        });
    }
);
