define(
    [   'app/app',
        'text!modules/demoDualStorage/templates/testpage.html',
        'marionette'
    ],
    function (app, template, Marionette) {
    return Marionette.ItemView.extend({
        template: template,
        events: {
            'click #submit': 'submitName'
        },
        ui: {
            name : 'input[name="userName"]'
        },
        submitName: function(){
            var newName = this.ui.name.val();
            this.model.set({
                name: newName
            });
            this.model.save();
        }
    });
});