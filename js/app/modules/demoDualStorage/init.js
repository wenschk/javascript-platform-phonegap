define(['app/app', 'modules/demoDualStorage/controller', 'marionette'], function(app, mainController, Marionette) {

    app.module("demoDualStorage", function(test){

        var router = null ;

        test.startWithParent = true;

        test.addInitializer(function(){
            router = new Marionette.AppRouter({
                controller: new mainController(),
                appRoutes: {
                    "demoDualStorage": 'test'
                }
            });
                        var i;

            console.log("local storage");
            for (i=0; i<localStorage.length; i++)   {
                console.log(localStorage.key(i)+"=["+localStorage.getItem(localStorage.key(i))+"]");
            }

            console.log("session storage");
            for (i=0; i<sessionStorage.length; i++) {
                console.log(sessionStorage.key(i)+"="+sessionStorage.getItem(sessionStorage.key(i))+"]");
            }


        });


    });


    return app;
});



