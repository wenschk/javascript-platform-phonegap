define([
    'app/app',
    'jquery',
    'layouts/main',
    'backbone',
    'config/settings',
    'modules/navigation/init',
    'modules/staticPage/views/homepage'  ,
    'modules/staticPage/init',
    'modules/demoDualStorage/init',
    'modules/demoLocalNotification/init',
   
    ],
    function (app, $, mainLayout, Backbone, config, navigation, homepage) {
        return app.addInitializer(function(options) {
            app.addRegions({
                mainRegion: "#main",
                headerRegion: "#header-part"
            });
            app.mainLayout = new mainLayout();
            app.mainRegion.show(app.mainLayout);           
            app.headerRegion.show(app.navigation.View());
            app.mainLayout.top.show(app.module("staticPage").defaultView());
            Backbone.history.start({
                pushState: true,
                root: "/"
            });
            app.navigation.navigate("home");

        });
    }
);