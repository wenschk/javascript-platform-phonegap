define([
    'app/app'],
    function (app) {
        var developmentServer = 'http://localhost:3000',
            productionServer = ' http://localhost',
            baseURL = developmentServer;

        app.serverUrl = baseURL+'/api/v1';
    return app
});
