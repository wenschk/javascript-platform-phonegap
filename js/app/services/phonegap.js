define([
    'app/app'
],
function (app) {
    var phonegap = {
        init: function(initializer) {
            document.addEventListener('deviceready', initializer.onDeviceReady, false);
            this.plugin_offline();
        },
        
        plugin_offline: function(){            
            document.addEventListener("offline", function(){
                app.vent.trigger('navigate:to', 'error');
            }, false);
            document.addEventListener("online", function(){
                app.vent.trigger('navigate:to', 'home');
            }, false);            
        }

    };

    return phonegap;
});
