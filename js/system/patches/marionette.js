define(['marionette'], function(Marionette) {
    //Override getTemplate to enable loading of templates with require js.
    //Maybe concider caching the templates that are already compiled in the future.
    _.extend(Marionette.View.prototype, {
        getTemplate : function () {
            return _.template(this.template);
        }
    });
});