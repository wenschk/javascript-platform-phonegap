define(['system/libraries/navigator/navigator'], function (navigator) {
    var contacts = navigator.contacts || null;
    if(contacts ) {
        if((typeof contacts.ContactFindOptions == "undefined")){
            contacts.ContactFindOptions = ContactFindOptions;
        }

    }else{
        //MOCK UP !!
        contacts = {
            ContactFindOptions : function() {
                return true;
            },
                find : function(a, b, c, d) {
                    b([
                        {displayName: 'test', emails : [{value: 'test@demo.fi'}]},
                        {displayName: 'kenneth', emails : [{value: 'kenneth@demo.fi'},{value: 'kenneth@test.com'}]},
                        {displayName: 'demo', emails : [{value: 'demo@demo.fi'}]},
                        {displayName: 'notfound', emails : [{value: 'notfound@demo.fi'}]}
                    ]);
                }
        }
    }

    return contacts;
});
