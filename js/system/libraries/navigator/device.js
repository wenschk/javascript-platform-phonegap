define(['system/libraries/navigator/navigator'], function (navigator) {

    var platform = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        iPhone: function() {
            return navigator.userAgent.match(/iPhone/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        WindowsMobile: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        isMobile: function() {
            return (platform.Android() || platform.BlackBerry() || platform.iOS() || platform.Opera() || platform.WindowsMobile());
        }
    };
    var device={
        platform: platform
    };


    return device;
});




