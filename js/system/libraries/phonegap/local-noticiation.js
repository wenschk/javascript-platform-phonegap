define(
function(){
    var notification = {
        init: function (){
            //window.plugin.notification.local.setDefaults({ autoCancel: true });   
        },
        
        add: function(){
            var now   = new Date().getTime(),
            _60_seconds_from_now = new Date(now + 60*1000);
            window.plugin.notification.local.add({
                id:      1,
                title:   'Reminder',
                message: 'Testing',
                repeat:  'weekly',
                date:    _60_seconds_from_now
            });            
        },
        cancel: function (){
            window.plugin.notification.local.cancel(ID, function () {
               // The notification has been canceled
           }, scope);           
        },
        onclick: function (){
            window.plugin.notification.local.onclick = function (id, state, json) {
                console.log(id, JSON.parse(json).test);
            }            
        }

    }
    return notification;
});
