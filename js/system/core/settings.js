define([
    'app/app'
],
function (app) {           
        var setup = {
            load: function (){
                this.ajaxError();
                this.ajaxCache(false);
            },
            ajaxError : function(){               
                $( document ).ajaxError(function(event, jqxhr, settings, exception) {
                var errorText="Server request error. StatusCode: "+ jqxhr.status;
                    switch(jqxhr.status){
                        case 0:
                              errorText += " "+"Server is unreachable";
                         break;
                }
                console.log(errorText);
                });
            },
            ajaxCache : function (bool){
              $.ajaxSetup({ cache: bool });  
            }
        };
        return setup;

});