
require.config({
    //By default load any module IDs from js/lib
    baseUrl: 'js',
    //except, if the module ID starts with "app",
    //load it from the js/app directory. paths
    //config is relative to the baseUrl, and
    //never includes a ".js" extension since
    //the paths config could be for a directory.
    paths: {
        app: 'app',
        layouts: 'app/layouts',
        initializers: 'app/initializers',
        config: 'app/config',
        services: 'app/services',
        modules: 'app/modules',
        backbone: 'system/core/backbone-min',
        underscore: 'system/core/underscore-min',
        marionette: 'system/core/backbone.marionette.min',
        backbone_babysitter: 'system/core/backbone.babysitter.min',
        backbone_eventbinder: 'system/core/backbone.eventbinder.min',
        backbone_wreqr: 'system/core/backbone.wreqr.min',
        bootstrap: 'system/libraries/bootstrap.min',
        jquery: 'system/libraries/jquery',
        text: 'system/core/text',
        async: 'system/core/async',
        patches: 'system/patches',
        navigator: "system/libraries/navigator",
        phonegap: "system/libraries/phonegap",
        extensions: "system/extensions",
        dualStorage: "system/extensions/backbone.dualstorage"
    },
    catchError: true,
    waitSeconds: 3,
    shim: {
        'backbone': {
            deps: ['underscore', 'jquery'],
            //Once loaded, use the global 'Backbone' as the
            //module value.
            exports: 'Backbone'
        },
        'underscore': {
            exports: '_'
        },
        'marionette': {
            deps: ['backbone', 'backbone_babysitter', 'backbone_eventbinder', 'backbone_wreqr'],
            exports: 'Marionette'
        },
        'backbone_babysitter': {
            deps: ['backbone'],
            exports: '_'
        },
        'backbone_eventbinder': {
            deps: ['backbone'],
            exports: '_'
        },
        'backbone_wreqr': {
            deps: ['backbone'],
            exports: '_'
        },
        'bootstrap': {
            deps: ['jquery']
        },
        'dualStorage': {
            deps: ['backbone']
        }

    }
});
requirejs.onError = function (err) {
    if (err.requireType == 'timeout') {
        alert(err);
        setInterval(function(){
            location.reload();
        },3000);

    } else {
        throw err;
    }
};
require(['patches/backbone', 'patches/marionette', 'dualStorage'], function() {
    require([
         "app/app",
         "jquery",
         "navigator/device",
         "services/phonegap",
         "system/core/settings",
         "bootstrap"
               
    ],  function(app, $, device, phoneGapService, settings) {
            var initializer = {
                initialize: function() {
                    $.support.cors = true;
                    app.device = device;
                    if(app.device.platform.isMobile()){                      
                        phoneGapService.init(this);
                    }else{
                        this.onDeviceReady();
                    }
                    settings.load();
                },
                onDeviceReady: function() {
                    require(["initializers/startup"], function (startup) {
                        app.start();
                    });
                }
            };
            initializer.initialize();
    }


    );
});


